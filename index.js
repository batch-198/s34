const express = require("express");
/*Mongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database*/
const mongoose = require("mongoose");
const app = express();

const port = 4000;

/*
	Mongoose Connection

	mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. first, the connection string to connect our api to our mongodb, second, is an object used to add information between mongoose and mongodb

	replace/change <password> in the connection string to your db user password

	just before the ? in the connection string, add the database name

*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.bewdg.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// We will create a notification if the connection to the db is a success or failed
let db = mongoose.connection;

// this is to show notification of an internal server error from MongoDB.
db.on('error', console.error.bind(console, "MongoDB Connection Error."));

// if the connection is open and successful, we will output a message in the terminal/gitbash

db.once('open',()=>console.log("Conntected to MongoDB"))

// express.json() to be able to handle the request body and parse it into JS Object
app.use(express.json());

// import our routes and use it as middleware
// which means, we will be able to group our routes

const courseRoutes = require('./routes/courseRoutes');
// console.log(courseRoutes);

// use our routes and group them together under '/courses'
// our endpoints are now prefaced with /courses
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

app.listen(port,()=>
	console.log(`Server is running at port 4000`));