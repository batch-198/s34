/*
	Naming convention for controllerse is that it should be named after the model/documents it is concerned with

	Controllers are functions which contain the actual business logic of our API and is triggered by a route
*/

// To create a controller, we first add it into our modules.exports
// So that we can import the controllers from our module

// import the User model in the controllers because this is where we're going to use it

const User = require("../models/User");

// Import Course
const Course = require("../models/Course");

// import bcrypt
const bcrypt = require("bcrypt");

// bcrypt is a package which allows us to hash out passwords to add a layer of security for our user's details

// import auth.js module to use createAccess Token and its subsequent methods
const auth = require("../auth");

module.exports.registerUser = (req,res)=>{

	// console.log(req.body)

	// using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash

	const hashedPw = bcrypt.hashSync(req.body.password,10);

	// console.log(hashedPw);

	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getUserDetails = (req,res)=>{
	// console.log(req.body);
	
	console.log(req.user) //contains the details of the logged in user 
	// find() will return an array of documents which matches the criteria. It will still return an array even if it only found 1 document

	// fineOne() will return a single document that matched our criteria
	// User.findOne({"id": req.body.id})

	// findBYId() - mongoose method that will find a document strictly by its ID
	
	// this will allow us to ensure that the logged in user or the user that passed the token will be able to get his own details and only his own

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{

	console.log(req.body);

	/*
		Steps for logging in our User:

		1. find the user by email
		2. If user is found, check if his password input and hashed password in db matches
		3. If user is not found, send a message to the client
		4. If upon checking the found user's password is the same as input pw from req.body, we will generate a "key" for the user to have authorization to access certain features in our app
	*/

	// mongo.db: db.users.findOne({email: req.body.email})

	User.findOne({email:req.body.email})
	.then(foundUser => {

		// foundUser is the parameter that contains the result of findOne
		// findOne() returns null if it is not able to match any document

		if(foundUser === null){
			return res.send({message: "No User Found."})
			// client will receive this object with our message if no user is found
		} else {
			// console.log(foundUser)
			// if we find a user, foundUser will contain the document that matched the input email

			// Check if input password from req.body matches the hashed pw in foundUser document

			/*
				Syntax for compareSync:

				bcrypt.compareSync(<inputString>,<hashedString>)

				If the inputString and hashedString matches and are the same, the compareSync will return trye.
				Else, it will return false.
			*/


			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)

			// console.log(isPasswordCorrect);

			// IF the password is correct, we will create a "key", a token for our user, else, we will send a message

			if(isPasswordCorrect){
				
				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our own module called auth.js.

					This module will create a encoded string which contains our user's details.

					This encoded is string is what we call a JSONWebToken
				*/

				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message: "Incorrect Password"});
			}
		}
	})
}

// Find User by E-mail, inputted in req body, accessible for ALL users
module.exports.checkEmail = (req,res) => {
	// console.log(req.body);
	User.findOne({email: req.body.email})
	.then(result => {

		// findOne will result null if no match is found
		if(result === null){
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(error => res.send(error))
}

module.exports.enroll = async (req,res) => {
	// check the id of user who will enroll
	// console.log(req.user.id);

	// check the id of the course we want to enroll
	// console.log(req.body.courseId);


	/*
		Enrollment Steps:

		1. Find the user and update his enrollment subdocument array. We will push the courseId in the enrollments array. 

		2. Find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

		Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting JS continue line per line

		aync and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behavior of running each code line by line, we will be able to wait for the results of a function

		To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding

	*/

	// Validate the user if they are admin or not
	// IF users is admin, send a message to client and end response.
	// ELSE, we will continue
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}

	// return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		// console.log(user);

		// add the courseId in an object and push that object into the user's enrollments. It is an object because we have to follow the schema of enrollments subdocument array
		let newEnrollment = {

			courseId: req.body.courseId
		}

		// access the enrollments array from our use and push the new enrollment subdocument intro the enrollments array.
		user.enrollments.push(newEnrollment)

		// we must save the user document and return the value of saving our document. then return IF we push the subdocument successfully. catch and return error if otherwise.

		return user.save().then(user => true).catch(err => err.message);
	})
	// console.log(isUserUpdated);
	// Add an IF statement and stop the process IF isUserUpdated DOES NOT contain true

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	// Find the course where we will enroll or add the user as enrollee and return true IF we were able to push the user into the enrollees array properly or send the error message instead

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		// should contain the found course or the course we want to enroll in
		// console.log(course);

		let enrollee = {
			userId: req.user.id
		}

		// push the enrollee into the enrollees subdocument array of the course:
		course.enrollees.push(enrollee);

		// save the course document
		// Return true IF we were able to save and add the user as enrollees properly
		// Return an err message if catch error

		return course.save().then(course => true).catch(err => err.message);
	})
	
	// console.log(isCourseUpdated);

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	// Ensure that we were able to update User and Course document to add our enrollment and enrollee respoectively, send a message to the client to end the enrollment process:

	if(isUserUpdated && isCourseUpdated){
		return res.send ({message: "Thank you for enrolling"})
	}
}