// Import the Course model so we can manipulate it and add a new course document
const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{
	
	// use the course model to connect to our collection and retrieves our courses
	// to be able to query into our collection we use the model connected to that collection
	// in MongoDB: db.courses.find({})
	// Model.find() - returns a collection of documents that matches our criteria, similar to mongoDB's .find()
	
	Course.find({})
	.then(result => res.send(result))
	.catch(result => res.send(error))
}

module.exports.addCourse = (req,res)=>{
	// console.log(req.body)

	// using the course model, we will use its constructor to create our Course document which will follow the schema of the model, and add method for document creation
	
	let newCourse = new Course ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	// console.log(newCourse);
	// newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor 

	// .save() method is added into our newCourse object. This will allow us to save the content of newCourse into our collecti

	// .then() allows us to process the result of the previous function/mathod in its own anonymous function

	// .catch() catches the error and allows us to process it and send to client

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.activeCourses = (req,res)=>{
	
	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getSingleCourse = (req,res) => {

	// console.log(req.params)
	// req.params is an object that contains the value captured via route params
	// the field name of the req.params indicate the name of the route params

	console.log(req.params.courseId)

	Course.findOne({id: req.params.courseId})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateCourse = (req,res) => {
	// check if we can get the id
	console.log(req.params.courseId);

	// check the update that is input
	console.log(req.body);

	// findByIdAndUpdate - used to update documents and has 3 arguments:
	// finbyByIdAndUpdate(<id>,{updates},{new:true})

	// we can create a new object to filter update details
	// fields/properties that are not part of the update object will not be updated

	let update = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.archiveCourse = (req,res) => {
	console.log(req.params.courseId);
	
	Course.findByIdAndUpdate(req.params.courseId,{"isActive":"false"},{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}