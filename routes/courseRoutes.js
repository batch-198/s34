/*
	to be able to create routes from another file to be used in out application, from here, we have to import Express as well. However we will now use another method from Express to contain our routes.

	The Router() method will allow us to contain our routes.
*/

const express = require("express");
const router = express.Router();


const courseControllers = require("../controllers/courseControllers");
// All routes to courses now has an endpoint prefaced with /courses

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// endpoint - /courses/
// Gets all course documents whether it is active or inactive
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

// endpoint - /courses/
// Only logged in user is able to use addCourse
// verifyAdmin() will disallow regular logged in and non-logged in users from using this route
router.post('/',verify,verifyAdmin,courseControllers.addCourse);

// Get all Active Courses - (Regular, Non-logged In)
router.get('/activeCourses',courseControllers.activeCourses);

// Find Course by ID, accessible for ALL users
// we can pass data in a route without the use of req body by passing a small data through the url with the use of route parameter

// http://localhost:4000/courses/getSingleCourse/62e750a4886bc8dfaaf58289
router.get('/getSingleCourse/:courseId',courseControllers.getSingleCourse);

// update a single course
// pass the id of the course we want to update via route params
// the update details will be passed via request body

router.put('/updateCourse/:courseId', verify,verifyAdmin,courseControllers.updateCourse);

// archive a single course
// pass the id for the course we want to update via route params
// we will directly update the course as inactive
router.delete('/archiveCourse/:courseId',verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;