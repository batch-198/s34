const express = require("express");
const router = express.Router();


// Routes should just contain endpoints and it should only trigger the function but it should not be where we define the logic of our function

// The business logic of our API should be in controllers
const userControllers = require("../controllers/userControllers");

// check the imported userControllers
// console.log(userControllers);

// import auth to be able to have access and use the verify method act as middleware for our routes.
// Middlewares add in the route such as verfy() will have access to the req,res objects
const auth = require("../auth");

// destructure auth to get only our methods and save it in variables

const {verify} = auth;

/*
	Updated Route Syntax:

	method("/endpoint",handlerFunction)
*/

// register
router.post('/',userControllers.registerUser);


// POST method route to get user details by id:

// verify() is used as a middlesware which means our request will get through verify first before our controller

// verify() will not only check the validity of the token but also add the decoded data of the token in the request object as req.user


router.get('/details',verify,userControllers.getUserDetails);

// Route for User Authentication
router.post('/login',userControllers.loginUser);

// Find User by E-mail, inputted in req body, accessible for ALL users
router.get('/checkEmail',userControllers.checkEmail);

// User Enrollment
// courseId will come from req.body
// userID will come from req.user
router.post('/enroll',verify,userControllers.enroll);

module.exports = router;